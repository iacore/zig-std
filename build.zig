const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});

    const optimize = b.standardOptimizeOption(.{});

    const test_filter = b.option([]const u8, "test-filter", "test filter");

    const mod = b.addModule("std", .{
        .root_source_file = b.path("lib/std/std.zig"),
        .target = target,
        .optimize = optimize,
    });
    _ = mod;

    const lib_unit_tests = b.addTest(.{
        .root_source_file = b.path("lib/std/std.zig"),
        .target = target,
        .optimize = optimize,
        .zig_lib_dir = .{ .path = "lib" },
        .filter = test_filter,
    });

    const run_lib_unit_tests = b.addRunArtifact(lib_unit_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_lib_unit_tests.step);
}
